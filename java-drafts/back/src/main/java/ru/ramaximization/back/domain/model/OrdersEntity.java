package ru.ramaximization.back.domain.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "orders", schema = "public", catalog = "ramaximization")
public class OrdersEntity {
    private Long id;
    private String orderNum;
    private Integer start;
    private Integer finish;
    private Integer startDate;
    private Integer dur;
    private Integer minUnit;
    private Integer maxUnit;
    private String tariff;
    private Integer needWagonModel;
    private Integer shortagePenalty;
    private Integer month;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "order_num")
    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    @Basic
    @Column(name = "start")
    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    @Basic
    @Column(name = "finish")
    public Integer getFinish() {
        return finish;
    }

    public void setFinish(Integer finish) {
        this.finish = finish;
    }

    @Basic
    @Column(name = "start_date")
    public Integer getStartDate() {
        return startDate;
    }

    public void setStartDate(Integer startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "dur")
    public Integer getDur() {
        return dur;
    }

    public void setDur(Integer dur) {
        this.dur = dur;
    }

    @Basic
    @Column(name = "min_unit")
    public Integer getMinUnit() {
        return minUnit;
    }

    public void setMinUnit(Integer minUnit) {
        this.minUnit = minUnit;
    }

    @Basic
    @Column(name = "max_unit")
    public Integer getMaxUnit() {
        return maxUnit;
    }

    public void setMaxUnit(Integer maxUnit) {
        this.maxUnit = maxUnit;
    }

    @Basic
    @Column(name = "tariff")
    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    @Basic
    @Column(name = "need_wagon_model")
    public Integer getNeedWagonModel() {
        return needWagonModel;
    }

    public void setNeedWagonModel(Integer needWagonModel) {
        this.needWagonModel = needWagonModel;
    }

    @Basic
    @Column(name = "shortage_penalty")
    public Integer getShortagePenalty() {
        return shortagePenalty;
    }

    public void setShortagePenalty(Integer shortagePenalty) {
        this.shortagePenalty = shortagePenalty;
    }

    @Basic
    @Column(name = "month")
    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrdersEntity that = (OrdersEntity) o;
        return Objects.equals(orderNum, that.orderNum) && Objects.equals(start, that.start) && Objects.equals(finish, that.finish) && Objects.equals(startDate, that.startDate) && Objects.equals(dur, that.dur) && Objects.equals(minUnit, that.minUnit) && Objects.equals(maxUnit, that.maxUnit) && Objects.equals(tariff, that.tariff) && Objects.equals(needWagonModel, that.needWagonModel) && Objects.equals(shortagePenalty, that.shortagePenalty) && Objects.equals(month, that.month);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderNum, start, finish, startDate, dur, minUnit, maxUnit, tariff, needWagonModel, shortagePenalty, month);
    }
}
