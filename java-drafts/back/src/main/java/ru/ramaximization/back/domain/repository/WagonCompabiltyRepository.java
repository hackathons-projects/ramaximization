package ru.ramaximization.back.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ramaximization.back.domain.model.MetricsEntity;
import ru.ramaximization.back.domain.model.WagonCompabiltyEntity;

public interface WagonCompabiltyRepository extends JpaRepository<WagonCompabiltyEntity, Long> {
}
