package ru.ramaximization.back.contorller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import ru.ramaximization.back.service.GraphService;

import javax.annotation.PostConstruct;

@RequiredArgsConstructor
@RestController
public class GraphController {

    private final GraphService graphService;

    @PostConstruct
    private void init() {
        int start = 32406, finish = 35608;
        int path = graphService.path(start, finish, 30);
        System.out.println(path);
    }

}
