package ru.ramaximization.back.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ramaximization.back.domain.model.MetricsEntity;

import java.util.List;

public interface MetricsRepository extends JpaRepository<MetricsEntity, Long> {

    List<MetricsEntity> findAllByNodeGroupAndFromNodeOrderByTime(Integer nodeGroup, Integer fromNode);

}
