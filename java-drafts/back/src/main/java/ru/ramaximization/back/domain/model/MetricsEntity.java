package ru.ramaximization.back.domain.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "metrics", schema = "public", catalog = "ramaximization")
public class MetricsEntity {
    private Long id;
    private Integer fromNode;
    private Integer toNode;
    private Integer nodeGroup;
    private Integer distance;
    private Integer time;
    private Integer priceUnit;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "from_node")
    public Integer getFromNode() {
        return fromNode;
    }

    public void setFromNode(Integer fromNode) {
        this.fromNode = fromNode;
    }

    @Basic
    @Column(name = "to_node")
    public Integer getToNode() {
        return toNode;
    }

    public void setToNode(Integer toNode) {
        this.toNode = toNode;
    }

    @Basic
    @Column(name = "node_group")
    public Integer getNodeGroup() {
        return nodeGroup;
    }

    public void setNodeGroup(Integer nodeGroup) {
        this.nodeGroup = nodeGroup;
    }

    @Basic
    @Column(name = "distance")
    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    @Basic
    @Column(name = "time")
    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    @Basic
    @Column(name = "price_unit")
    public Integer getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(Integer priceUnit) {
        this.priceUnit = priceUnit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetricsEntity that = (MetricsEntity) o;
        return Objects.equals(fromNode, that.fromNode) && Objects.equals(toNode, that.toNode) && Objects.equals(nodeGroup, that.nodeGroup) && Objects.equals(distance, that.distance) && Objects.equals(time, that.time) && Objects.equals(priceUnit, that.priceUnit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fromNode, toNode, nodeGroup, distance, time, priceUnit);
    }
}
