package ru.ramaximization.back.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.math3.util.Pair;
import org.springframework.stereotype.Service;
import ru.ramaximization.back.domain.model.MetricsEntity;
import ru.ramaximization.back.domain.repository.MetricsRepository;
import ru.ramaximization.back.domain.repository.OrdersRepository;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicInteger;

@RequiredArgsConstructor
@Service
public class GraphService {

    private final static int NODE_GROUP = 100;

    private final OrdersRepository ordersRepository;
    private final MetricsRepository metricsRepository;


    public int path(int start, int finish, int threshold) {

        AtomicInteger result = new AtomicInteger(0);

        Deque<Pair<Integer, Integer>> stack = new ConcurrentLinkedDeque<>();
        Set<String> mark = new ConcurrentSkipListSet<>();

        stack.push(Pair.create(start, 0));

        while (!stack.isEmpty()) {
            Pair<Integer, Integer> pop = stack.pop();
            Integer node = pop.getFirst();
            Integer time = pop.getSecond();
            List<MetricsEntity> nodes = metricsRepository.findAllByNodeGroupAndFromNodeOrderByTime(NODE_GROUP, node);

            nodes.parallelStream()
                    .filter(metricsEntity -> metricsEntity.getTime() < threshold - time)
                    .forEach(metricsEntity -> {
                        if (!mark.contains(metricsEntity.getFromNode() + "-" + metricsEntity.getToNode())) {
                            int newTime = time + metricsEntity.getTime();
                            if (metricsEntity.getToNode().equals(finish) && result.getAcquire() >= newTime) {
                                result.setRelease(newTime);
                            } else {
                                stack.push(Pair.create(metricsEntity.getToNode(), newTime));
                                mark.add(metricsEntity.getFromNode() + "-" + metricsEntity.getToNode());
                            }
                        }
                    });

        }

        return result.get();

    }
}
