package ru.ramaximization.back.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ramaximization.back.domain.model.StandplacesEntity;

public interface StandplacesRepository extends JpaRepository<StandplacesEntity, Long> {
}
