package ru.ramaximization.back.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ramaximization.back.domain.model.SourcesEntity;

public interface SourcesRepository extends JpaRepository<SourcesEntity, Long> {
}
