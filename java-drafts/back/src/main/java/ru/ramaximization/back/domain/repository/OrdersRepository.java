package ru.ramaximization.back.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.ramaximization.back.domain.model.OrdersEntity;

import java.sql.ResultSet;
import java.util.List;

public interface OrdersRepository extends JpaRepository<OrdersEntity, Long> {

/*    @Query(value = "with d1 as (select *, split_part(tariff,':',1) as cur_tariff from orders where start_date=:start_date and month = 7), " +
            "d2 as (select *, split_part(tariff,':',2) as cur_tariff from orders where start_date=(:start_date - 1) and month = 7 and split_part(tariff,':',2) != ''), " +
            "d3 as (select *, split_part(tariff,':',3) as cur_tariff from orders where start_date=(:start_date - 2) and month = 7 and split_part(tariff,':',3) != ''), " +
            "d4 as (select *, split_part(tariff,':',4) as cur_tariff from orders where start_date=(:start_date - 3) and month = 7 and split_part(tariff,':',4) != '') " +
            "select id, order_num, start, finish, start_date, dur, min_unit, max_unit, cur_tariff::int4 as tariff, need_wagon_model, shortage_penalty, month from d1 " +
            "union all SELECT id, order_num, start, finish, start_date, dur, min_unit, max_unit, cur_tariff::int4 as tariff, need_wagon_model, shortage_penalty, month from d2 " +
            "union all SELECT id, order_num, start, finish, start_date, dur, min_unit, max_unit, cur_tariff::int4 as tariff, need_wagon_model, shortage_penalty, month from d3 " +
            "union all SELECT id, order_num, start, finish, start_date, dur, min_unit, max_unit, cur_tariff::int4 as tariff, need_wagon_model, shortage_penalty, month from d4 " +
            "order by tariff desc;", nativeQuery = true)
    ResultSet getOrdersWindow(@Param("start_date") Integer startDate);*/
}
