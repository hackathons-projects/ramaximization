package ru.ramaximization.back.domain.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "sources", schema = "public", catalog = "ramaximization")
public class SourcesEntity {
    private Long id;
    private Integer station;
    private Integer date;
    private Integer units;
    private Integer stand;
    private Integer wagonModel;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "station")
    public Integer getStation() {
        return station;
    }

    public void setStation(Integer station) {
        this.station = station;
    }

    @Basic
    @Column(name = "date")
    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    @Basic
    @Column(name = "units")
    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    @Basic
    @Column(name = "stand")
    public Integer getStand() {
        return stand;
    }

    public void setStand(Integer stand) {
        this.stand = stand;
    }

    @Basic
    @Column(name = "wagon_model")
    public Integer getWagonModel() {
        return wagonModel;
    }

    public void setWagonModel(Integer wagonModel) {
        this.wagonModel = wagonModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SourcesEntity that = (SourcesEntity) o;
        return Objects.equals(station, that.station) && Objects.equals(date, that.date) && Objects.equals(units, that.units) && Objects.equals(stand, that.stand) && Objects.equals(wagonModel, that.wagonModel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(station, date, units, stand, wagonModel);
    }
}
