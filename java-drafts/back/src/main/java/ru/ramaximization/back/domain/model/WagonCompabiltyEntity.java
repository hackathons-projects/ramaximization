package ru.ramaximization.back.domain.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "wagon_compabilty", schema = "public", catalog = "ramaximization")
public class WagonCompabiltyEntity {
    private Long id;
    private Integer needWagonModel;
    private Integer compatibleWagonModel;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "need_wagon_model")
    public Integer getNeedWagonModel() {
        return needWagonModel;
    }

    public void setNeedWagonModel(Integer needWagonModel) {
        this.needWagonModel = needWagonModel;
    }

    @Basic
    @Column(name = "compatible_wagon_model")
    public Integer getCompatibleWagonModel() {
        return compatibleWagonModel;
    }

    public void setCompatibleWagonModel(Integer compatibleWagonModel) {
        this.compatibleWagonModel = compatibleWagonModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WagonCompabiltyEntity that = (WagonCompabiltyEntity) o;
        return Objects.equals(needWagonModel, that.needWagonModel) && Objects.equals(compatibleWagonModel, that.compatibleWagonModel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(needWagonModel, compatibleWagonModel);
    }
}
