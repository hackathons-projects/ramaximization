package ru.ramaximization.back.domain.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "standplaces", schema = "public", catalog = "ramaximization")
public class StandplacesEntity {
    private Long id;
    private Integer station;
    private Integer pricePut;
    private Integer priceStand;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "station")
    public Integer getStation() {
        return station;
    }

    public void setStation(Integer station) {
        this.station = station;
    }

    @Basic
    @Column(name = "price_put")
    public Integer getPricePut() {
        return pricePut;
    }

    public void setPricePut(Integer pricePut) {
        this.pricePut = pricePut;
    }

    @Basic
    @Column(name = "price_stand")
    public Integer getPriceStand() {
        return priceStand;
    }

    public void setPriceStand(Integer priceStand) {
        this.priceStand = priceStand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StandplacesEntity that = (StandplacesEntity) o;
        return Objects.equals(station, that.station) && Objects.equals(pricePut, that.pricePut) && Objects.equals(priceStand, that.priceStand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(station, pricePut, priceStand);
    }
}
