with d1 as (select *, split_part(tariff,':',1) as cur_tariff from orders where start_date=:start_date and month = 7),
     d2 as (select *, split_part(tariff,':',2) as cur_tariff from orders where start_date=(:start_date - 1) and month = 7 and split_part(tariff,':',2) != ''),
    d3 as (select *, split_part(tariff,':',3) as cur_tariff from orders where start_date=(:start_date - 2) and month = 7 and split_part(tariff,':',3) != ''),
    d4 as (select *, split_part(tariff,':',4) as cur_tariff from orders where start_date=(:start_date - 3) and month = 7 and split_part(tariff,':',4) != '')

select order_num, start, finish, start_date, dur, max_unit as unit, need_wagon_model, cur_tariff::int4 as tariff
from d1
union all SELECT order_num, start, finish, start_date, dur, max_unit as unit, need_wagon_model, cur_tariff::int4 as tariff from d2
union all SELECT order_num, start, finish, start_date, dur, max_unit as unit, need_wagon_model, cur_tariff::int4 as tariff from d3
union all SELECT order_num, start, finish, start_date, dur, max_unit as unit, need_wagon_model, cur_tariff::int4 as tariff from d4
order by tariff desc
;
