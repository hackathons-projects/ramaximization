from pyomo.environ import *
from math import floor, ceil, sqrt
from collections import defaultdict, deque
from neo4j import GraphDatabase

from openpyxl import load_workbook


import pandas as pd
import numpy as np
import random
import networkx as nx

random.seed(42)
CONST_INF = 10**10

class MetricsGraph:
    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    def print_metrics(self, start, finish, cost):
        with self.driver.session() as session:
            metrics = session.write_transaction(self._return_metrics, start, finish, cost)
            print(metrics)
            return metrics

    @staticmethod
    def _return_metrics(tx, start, finish, cost):
        result = tx.run(
            "MATCH (s:Metrics {Name: $start} ), (f:Metrics {Name: $finish}),"
            "p = shortestPath((s)-[rels:REL*]-(f)) "
            "WHERE (REDUCE(s = 0, r IN rels | s + TOINTEGER(r.Time)) < $cost)"
            "RETURN REDUCE(s = 0, r IN rels | s + TOINTEGER(r.Time))", start=start, finish=finish, cost=cost)
        return result.single()[0]


metrics_hash = {}


def get_metrics(start, finish, cost, host="bolt://localhost:7687", user="neo4j", pwd="123456"):
    if (start, finish) not in metrics_hash:
        metrics_class = MetricsGraph("bolt://localhost:7687", "neo4j", "123456")
        metrics = metrics_class.print_metrics(start, finish, cost)
        metrics_class.close()
        metrics_hash[(start, finish)] = metrics
    return metrics_hash[(start, finish)]

class Wagon:
    def __init__(self, initial_position, start_date, model):
        self.initial_position = initial_position
        self.start_date = start_date
        self.model = model
        
class Order:
    def __init__(self, start, finish, start_date, duration,
                 min_unit, max_unit, tariff, wagon_models, shortage_penalty, index):
        self.start = start
        self.finish = finish
        self.start_date = start_date
        self.duration = duration
        self.min_unit = min_unit
        self.max_unit = max_unit
        self.tariff = tariff
        self.wagon_models = wagon_models
        self.shortage_penalty = shortage_penalty
        self.index = index

            
def obtain_tariff(tariff_string):
    if type(tariff_string) != int:
        return list(map(int, tariff_string.split(':')))
    else:
        return [int(tariff_string)]
        
def read_data():
    standplaces = pd.read_excel('./input/standplaces.xlsx')
    WagonModelCompatibility = pd.read_excel('./input/WagonModelCompatibility.xlsx')
    
    compatible_models = defaultdict(list)
    for i in range(WagonModelCompatibility.shape[0]):
        compatible_models[WagonModelCompatibility.NeedWagonModel[i]].append(WagonModelCompatibility.CompatibleWagonModel[i])
    
    Metrics = pd.read_excel('./input/Metrics.xlsx')
    relocation_graph = nx.DiGraph()
    relocation_graph.add_weighted_edges_from([(Metrics.From[i], Metrics.To[i], Metrics.Time[i]) for i in range(Metrics.shape[0])])
    
    for i in range(Metrics.shape[0]):
        relocation_graph[Metrics.From[i]][Metrics.To[i]]['price'] = Metrics.PriceUnit[i]
    
    orders07 = pd.read_excel('./input/Orders_07.xlsx')
    orders08 = pd.read_excel('./input/Orders_08.xlsx')
    
    orders = []
    for i in range(orders07.shape[0]):
        orders.append(
            Order(
                start = orders07.Start[i],
                finish = orders07.Finish[i],
                start_date = orders07.StartDate[i],
                duration = orders07.Dur[i],
                min_unit = orders07.MinUnit[i],
                max_unit = orders07.MaxUnit[i],
                tariff = obtain_tariff(orders07.Tariff[i]),
                wagon_models = [orders07.NeedWagonModel[i]] + compatible_models[orders07.NeedWagonModel[i]],
                shortage_penalty = orders07.ShortagePenalty[i],
                index = i
            )
        )
    
    for i in range(orders08.shape[0]):
        orders.append(
            Order(
                start = orders08.Start[i],
                finish = orders08.Finish[i],
                start_date = orders08.StartDate[i] + 30,
                duration = orders08.Dur[i],
                min_unit = orders08.MinUnit[i],
                max_unit = orders08.MaxUnit[i],
                tariff = obtain_tariff(orders08.Tariff[i]),
                wagon_models = [orders08.NeedWagonModel[i]] + compatible_models[orders08.NeedWagonModel[i]],
                shortage_penalty = None,
                index = i
            )
        )
    
    sources = pd.read_excel('./input/sources.xlsx')
    stand_penalty = defaultdict(int)
    wagons = []
    
    for i in range(sources.shape[0]):
        stand_penalty[sources.Station[i]] = min(sources[sources.Station == sources.Station[i]].Stand)
        for j in range(sources.Units[i]):
            wagons.append(
                Wagon(
                    initial_position = sources.Station[i],
                    start_date = sources.Date[i],
                    model = sources.WagonModel[i]
                )
            )
    
    return standplaces, compatible_models, relocation_graph, orders, sources, stand_penalty, wagons

def generate_paths(wagons, orders, sources, relocation_graph, stand_penalty):
    all_paths_for_wagons = []
    all_paths_orders = []
    all_paths_profit = []
    
    paths_for_orders = defaultdict(list)
    
    cached_relocations = {}
    
    counter = 0
    for i in range(sources.shape[0]):
        if i % 100 == 0:
            print('source number ', i)
        paths_for_wagon, paths_orders, paths_profit = generate_paths_for_wagon(wagons[counter], orders, relocation_graph, stand_penalty, cached_relocations)
        
        for j in range(sources.Units[i]):
            all_paths_for_wagons.append(paths_for_wagon)
            all_paths_orders.append(paths_orders)
            all_paths_profit.append(paths_profit)
            
            for k, path_orders in enumerate(paths_orders):
                for (index, shortage) in path_orders:
                    paths_for_orders[(index, shortage)].append((counter + j, k))
            
        counter += sources.Units[i]
    
    return all_paths_for_wagons, all_paths_orders, all_paths_profit, paths_for_orders

def one_free_step_relocation(initial_position, total_time, relocation_graph, stand_penalty):
    minimal_relocation_cost = total_time * stand_penalty[initial_position]
    minimal_relocation_path = [initial_position]
    
    if not relocation_graph.has_node(initial_position):
        return minimal_relocation_cost, minimal_relocation_path
    
    for neighbor in relocation_graph[initial_position]:
        relocation_cost = relocation_graph[initial_position][neighbor]['price'] + min(stand_penalty[initial_position], stand_penalty[neighbor]) * max(total_time - relocation_graph[initial_position][neighbor]['weight'], 0)
        if relocation_cost < minimal_relocation_cost:
            minimal_relocation_cost = relocation_cost
            minimal_relocation_path = [initial_position, neighbor]
        
    return minimal_relocation_cost, minimal_relocation_path

def one_step_feasible_orders(initial_position, initial_time, wagon_model, orders, relocation_graph, stand_penalty, cached_relocations, max_shortage=0):
    if cached_relocations.get((initial_position, initial_time, wagon_model)) is not None:
        return cached_relocations[(initial_position, initial_time, wagon_model)]
    
    time_distances = {}
    if relocation_graph.has_node(initial_position):
        time_distances = {neighbor: relocation_graph[initial_position][neighbor]['weight'] for neighbor in relocation_graph.neighbors(initial_position)} 
    
    time_distances[initial_position] = 0
    
    # списки допустимых заказов в зависимости от опоздания
    neighbor_orders_by_shortage = [[next_order for next_order in orders if (next_order.start in time_distances) and (next_order.start_date + shortage >= initial_time + time_distances[next_order.start])
                                                    and (wagon_model in next_order.wagon_models) and (shortage <= len(next_order.tariff))] for shortage in range(max_shortage + 1)]
    
    relocation_costs = [[(order.start_date + shortage - initial_time - time_distances[order.start]) * min(stand_penalty[initial_position], stand_penalty[order.start]) for order in neighbor_orders_by_shortage[shortage]] for shortage in range(max_shortage + 1)]
    relocation_paths = [[[initial_position, order.start] for order in neighbor_orders_by_shortage[shortage]] for shortage in range(max_shortage + 1)]
    
    for shortage in range(max_shortage + 1):
        for i, order in enumerate(neighbor_orders_by_shortage[shortage]):
            if relocation_graph.has_edge(initial_position, order.start):
                relocation_costs[shortage][i] += relocation_graph[initial_position][order.start]['price']
    
    cached_relocations[(initial_position, initial_time, wagon_model)] = (relocation_costs, relocation_paths, neighbor_orders_by_shortage)
    
    return relocation_costs, relocation_paths, neighbor_orders_by_shortage
    
def generate_paths_for_wagon(wagon, orders, relocation_graph, stand_penalty, cached_relocations, planning_horizon=60, max_number_orders=1, max_shortage=0):
    
    paths_for_wagon = []
    paths_orders = []
    paths_profit = []
    
    #pr = cProfile.Profile()
    #pr.enable()
    
    # 0 заказов в пути для вагона
    zero_orders_cost, zero_orders_path = one_free_step_relocation(wagon.initial_position, planning_horizon - wagon.start_date, relocation_graph, stand_penalty)
    
    paths_for_wagon.append(zero_orders_path)
    paths_orders.append([])
    paths_profit.append(-zero_orders_cost)
    
    
    
    # 1 заказ в пути
    relocation_costs, relocation_paths, neighbor_orders_by_shortage = one_step_feasible_orders(wagon.initial_position, wagon.start_date, wagon.model, orders, relocation_graph, stand_penalty, cached_relocations, max_shortage=0)
    for shortage in range(max_shortage + 1):
        for i, order in enumerate(sorted(neighbor_orders_by_shortage[shortage], key=lambda x: -x.tariff[shortage])[0:25]):
            end_cost, end_path = one_free_step_relocation(order.finish, planning_horizon - order.start_date - order.duration - shortage, relocation_graph, stand_penalty)
            
            if order.tariff[shortage] - relocation_costs[shortage][i] - end_cost > 0:
                paths_for_wagon.append(relocation_paths[shortage][i] + [order] + end_path)
                paths_orders.append([(order.index, shortage)])
                paths_profit.append(order.tariff[shortage] - relocation_costs[shortage][i] - end_cost)
            
        
        for i, order in enumerate(sorted(neighbor_orders_by_shortage[shortage], key=lambda x: -x.tariff[shortage] / x.duration + 100 * x.start_date)[0:25]):        
            # 2 заказа в пути
            relocation_costs2, relocation_paths2, neighbor_orders_by_shortage2 = one_step_feasible_orders(order.finish, order.start_date + order.duration + shortage, wagon.model, orders, relocation_graph, stand_penalty, cached_relocations, max_shortage=0)
            
            for shortage2 in range(max_shortage + 1):
                for i2, order2 in enumerate(sorted(neighbor_orders_by_shortage2[shortage2], key=lambda x: -x.tariff[shortage2] / x.duration)[0:25]):
                    end_cost, end_path = one_free_step_relocation(order2.finish, planning_horizon - order2.start_date - order2.duration - shortage2, relocation_graph, stand_penalty)
                     
                    if order.tariff[shortage] + order2.tariff[shortage2] - relocation_costs[shortage][i] - relocation_costs2[shortage2][i2] - end_cost > 0:
                        paths_for_wagon.append(relocation_paths[shortage][i] + [order] + relocation_paths2[shortage2][i2] + [order2] + end_path)
                        paths_orders.append([(order.index, shortage), (order2.index, shortage2)])
                        paths_profit.append(order.tariff[shortage] + order2.tariff[shortage2] - relocation_costs[shortage][i] - relocation_costs2[shortage2][i2] - end_cost)
                
                for i2, order2 in enumerate(sorted(neighbor_orders_by_shortage2[shortage2], key=lambda x: -x.tariff[shortage2] / x.duration + 100 * x.start_date)[0:25]):    
                    # 3 заказа в пути
                    relocation_costs3, relocation_paths3, neighbor_orders_by_shortage3 = one_step_feasible_orders(order2.finish, order2.start_date + order2.duration + shortage2, wagon.model, orders, relocation_graph, stand_penalty, cached_relocations, max_shortage=0)
                    
                    for shortage3 in range(max_shortage + 1):
                        for i3, order3 in enumerate(sorted(neighbor_orders_by_shortage3[shortage3], key=lambda x: -x.tariff[shortage3] / x.duration)):
                            end_cost, end_path = one_free_step_relocation(order3.finish, planning_horizon - order3.start_date - order3.duration - shortage3, relocation_graph, stand_penalty)
                            
                            if order.tariff[shortage] + order2.tariff[shortage2] + order3.tariff[shortage3] - relocation_costs[shortage][i] - relocation_costs2[shortage2][i2] - relocation_costs3[shortage3][i3] - end_cost > 0:
                                paths_for_wagon.append(relocation_paths[shortage][i] + [order] + relocation_paths2[shortage2][i2] + [order2] + relocation_paths3[shortage3][i3] + [order3] + end_path)
                                paths_orders.append([(order.index, shortage), (order2.index, shortage2), (order3.index, shortage3)])
                                paths_profit.append(order.tariff[shortage] + order2.tariff[shortage2] + order3.tariff[shortage3] - relocation_costs[shortage][i] - relocation_costs2[shortage2][i2] - relocation_costs3[shortage3][i3] - end_cost)
                            
                            # 4 заказа в пути
                            #relocation_costs4, relocation_paths4, neighbor_orders_by_shortage4 = one_step_feasible_orders(order3.finish, order3.start_date + order3.duration + shortage3, wagon.model, orders, relocation_graph, stand_penalty, cached_relocations, max_shortage=0)
                            
                            #if len(neighbor_orders_by_shortage4[0]) > 0:
                            #    print('neighbors4', len(neighbor_orders_by_shortage4[0]))
                    
    #print('path profits =', paths_profit)
    #print('number of paths:', len(paths_profit))
    
    #pr.disable()
    #s = io.StringIO()
    #sortby = SortKey.CUMULATIVE
    #ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
    #ps.print_stats()
    #print(s.getvalue())
    
    return paths_for_wagon, paths_orders, paths_profit

def solve_model(orders, wagons, paths_for_orders, all_paths_for_wagons, all_paths_profit, all_paths_orders):
    indexing_set = [(j, k) for j in range(len(wagons)) for k in range(len(all_paths_for_wagons[j]))]
    
    orders_indexing_set = [(i, shortage) for i in range(len(orders)) for shortage in range(len(orders[i].tariff))]
    
    model = ConcreteModel()
    model.cons = ConstraintList()
    
    model.p = Var(indexing_set, within=Binary)
    model.orders = Var(orders_indexing_set, within=Binary)
    
    # каждый вагон должен выбрать один путь
    for j in range(len(wagons)):
        model.cons.add(sum(model.p[j, k] for k in range(len(all_paths_for_wagons[j]))) == 1)
    
    # условия на минимальный и максимальный размер состава для заказа
    for (i, shortage) in orders_indexing_set:
        model.cons.add(sum(model.p[j, k] for (j, k) in paths_for_orders[(i, shortage)]) <= orders[i].max_unit * model.orders[i, shortage])
        model.cons.add(orders[i].min_unit * model.orders[i, shortage] <= sum(model.p[j, k] for (j, k) in paths_for_orders[(i, shortage)]))
        
        if len(set(j for (j, k) in paths_for_orders[(i, shortage)])) < orders[i].min_unit:
            model.cons.add(model.orders[i, shortage] == 0)
            #print('order number', i, 'with shortage', shortage, 'was impossible')
        elif len(paths_for_orders[(i, shortage)]) < orders[i].min_unit:
            for (j, k) in paths_for_orders[(i, shortage)]:
                model.cons.add(model.orders[i, shortage] == model.p[j, k])
            #print('order number', i, 'with shortage', shortage, 'had just enough wagons')
    
    # заказ может быть выполнен только в один день
    for i in range(len(orders)):
        model.cons.add(sum(model.orders[i, shortage] for shortage in range(len(orders[i].tariff))) <= 1)
    
    # целевая функция - сумма профита по путям
    objective_function = sum(all_paths_profit[j][k] * model.p[j, k] for (j, k) in indexing_set)
    
    model.obj = Objective(expr=objective_function, sense=maximize)
    
    solver = SolverFactory('gurobi')
    
    solver.options['seconds'] = 100
    #solver.options['allowableGap'] = 0.01
    #solver.options['rins'] = 'on'
    
    model.write('wagons.mps', io_options = {"symbolic_solver_labels": True})
    
    solver.solve(model, tee=True)
    solver_objective = value(model.obj)
    
    completed_orders, selected_paths = read_solver_solution(model, indexing_set, orders_indexing_set)
    
    return model, completed_orders, selected_paths
    
def read_solver_solution(model, indexing_set, orders_indexing_set):
    selected_paths = {}
    completed_orders = [(i, shortage) for (i, shortage) in orders_indexing_set if value(model.orders[i, shortage]) > 0.9]
    
    for (j, k) in indexing_set:
        if value(model.p[j, k]) > 0.9:
            selected_paths[j] = k
    
    return completed_orders, selected_paths

def analyze_path(path, wagon, path_orders, orders, stand_penalty, relocation_graph):
    wagon_stand_penalty = 0
    wagon_relocation_penalty = 0
    wagon_relocations = 0
    
    
    wagon_empty_moves = pd.DataFrame(columns = ['Отправление', 'Прибытие', 'Тип простоя', 'Модель вагона', 'День отправления (от начала расчета)', 
                                            'День прибытия (от начала расчета)', 'Количество'	'Цена порожнего пробега (за вагон)', 
                                            'Код следующего заказа'	'Расчитанный тариф следующего заказа']
    )   
    start_point = wagon.initial_position
    date = wagon.start_date
    
    cnt = 0
    for (i, shortage) in path_orders:
        if orders[i].start_date <= 30:
            wagon_stand_penalty += min(stand_penalty[start_point], stand_penalty[orders[i].start]) * (orders[i].start_date + shortage - date)
            
            extra_stay = 0
            if stand_penalty[start_point] < stand_penalty[orders[i].start]:
                extra_stay = (orders[i].start_date + shortage - date)
            
            if start_point != orders[i].start:
                wagon_relocations += 1
                if relocation_graph.has_edge(start_point, orders[i].start):
                    wagon_relocation_penalty += relocation_graph[start_point][orders[i].start]['price']
                    
                    wagon_stand_penalty -= min(stand_penalty[start_point], stand_penalty[orders[i].start]) * relocation_graph[start_point][orders[i].start]['weight']
                    extra_stay -= relocation_graph[start_point][orders[i].start]['weight']
                
                    wagon_empty_moves.loc[cnt] = pd.DataFrame([[
                        start_point,
                        orders[i].start,
                        'P',
                        wagon.model,
                        date + extra_stay,
                        date + extra_stay + relocation_graph[start_point][orders[i].start]['weight'],
                        1,
                        relocation_graph[start_point][orders[i].start]['price'],
                        i,
                        orders[i].tariff[shortage]
                    ]], columns=['Отправление', 'Прибытие', 'Тип простоя', 'Модель вагона', 'День отправления (от начала расчета)', 
                                            'День прибытия (от начала расчета)', 'Количество', 'Цена порожнего пробега (за вагон)', 
                                            'Код следующего заказа', 'Расчитанный тариф следующего заказа'])
                    cnt += 1
        
            date = orders[i].start_date + orders[i].duration + shortage
            start_point = orders[i].finish
    
    wagon_stand_penalty += max(0, 30 - start_point) * min(stand_penalty[start_point], stand_penalty[path[-1]])
    
    return wagon_stand_penalty, wagon_relocation_penalty, wagon_relocations, wagon_empty_moves

def output_structures(completed_orders, selected_paths, orders, wagons, paths_for_orders, all_paths_for_wagons, all_paths_orders, stand_penalty, relocation_graph):
    priority_total_orders = defaultdict(int)
    priority_completed_orders = defaultdict(int)
    priority_completed_wagons = defaultdict(int)
    priority_percentage_completed = defaultdict(int)
    priority_average_premium = defaultdict(int)
    priority_total_premium = defaultdict(int)
    
    total_stand_penalty = 0
    total_relocation_penalty = 0
    total_relocations = 0
    
    empty_moves = pd.DataFrame(columns = ['Отправление', 'Прибытие', 'Тип простоя', 'Модель вагона', 'День отправления (от начала расчета)', 
                                            'День прибытия (от начала расчета)', 'Количество', 'Цена порожнего пробега (за вагон)', 
                                            'Код следующего заказа', 'Расчитанный тариф следующего заказа']
    )   
    
    for i, order in enumerate(orders):
        if order.start_date <= 30:
            priority_total_orders[order.shortage_penalty] += 1
    
    indices_of_completed = []
    
    for (i, shortage) in completed_orders:
        if orders[i].start_date <= 30:
            indices_of_completed.append(i)
            
            priority_completed_orders[orders[i].shortage_penalty] += 1
            priority_total_premium[orders[i].shortage_penalty] += orders[i].tariff[shortage]
            
            priority_completed_wagons[orders[i].shortage_penalty] += sum([(selected_paths[j] == k) for (j,k) in paths_for_orders[(i, shortage)]])
        
    for j in range(len(wagons)):
        wagon_stand_penalty, wagon_relocation_penalty, wagon_relocations, wagon_empty_moves = analyze_path(all_paths_for_wagons[j][selected_paths[j]], wagons[j], all_paths_orders[j][selected_paths[j]], orders, stand_penalty, relocation_graph)
        
        total_stand_penalty += wagon_stand_penalty
        total_relocation_penalty += wagon_relocation_penalty
        total_relocations += wagon_relocations
        
        empty_moves.append(wagon_empty_moves)
        
        
    orders_output = pd.read_excel('./input/Orders_07.xlsx')
    orders_output = orders_output.iloc[indices_of_completed]
    
    orders_output['StartDatePlan'] = orders_output['StartDate']
    orders_output['Tariff used'] = [orders[i].tariff for i in indices_of_completed]
    
    
    KPI_rows = [
        ['всего заказов'] + [priority_total_orders[i] for i in range(4)] + [sum([priority_total_orders[i] for i in range(4)])],
        ['Выполненных заказов в кол-ве'] + [priority_completed_orders[i] for i in range(4)] + [sum([priority_completed_orders[i] for i in range(4)])],
        ['Выполненных заказов в вагонах'] + [priority_completed_wagons[i] for i in range(4)] + [sum([priority_completed_wagons[i] for i in range(4)])],
        ['% выполнения'] + [priority_completed_orders[i] / (priority_total_orders[i] +1e-6) for i in range(4)] + [sum([priority_completed_orders[i] for i in range(4)]) / (1e-6 + sum([priority_total_orders[i] for i in range(4)]))],
        ['Средняя премия'] + [priority_total_premium[i] / (priority_completed_orders[i] + 1e-6) for i in range(4)] + [sum([priority_total_premium[i] for i in range(4)]) / (1e-6 + sum([priority_total_orders[i] for i in range(4)]))],
        ['Суммарная премия'] + [priority_total_premium[i] for i in range(4)] + [sum([priority_total_premium[i] for i in range(4)])],
        ['Кол-во порожних пробегов', '', '', '', '', total_relocations],
        ['Суммарная стоимость порожних пробегов', '', '', '', '', total_relocation_penalty],
        ['Кол-во вагново отправленных на станции отстоя', '', '', '', '', 0],
        ['Суммарная стоимость простоев', '', '', '', '', total_stand_penalty],
        ['Общие расходы (порожние пробеги + простой)', '', '', '', '', total_stand_penalty + total_relocation_penalty],
        ['', '', '', '', '', ''],
        ['', '', '', '', '', ''],
    ]   
    
    return KPI_rows, orders_output, empty_moves



# пример данных
    # arr = [
    #     ['всего заказов', 1, 2, 3, 4, 5],
    #     ['Выполненных заказов в кол-ве', 1, 2, 3, 4, 5],
    #     ['Выполненных заказов в вагонах', 1, 2, 3, 4, 5],
    #     ['% выполнения', 1, 2, 3, 4, 5],
    #     ['Средняя премия', '', '', '', '', 5],
    #     ['Суммарная премия', '', '', '', '', 5],
    #     ['Кол-во порожних пробегов', '', '', '', '', 5],
    #     ['Суммарная стоимость порожних пробегов', '', '', '', '', 5],
    #     ['Кол-во вагново отправленных на станции отстоя', '', '', '', '', 5],
    #     ['Суммарная стоимость простоев', '', '', '', '', 5],
    #     ['Общие расходы (порожние пробеги + простой)', '', '', '', '', 5],
    #     ['', '', '', '', '', ''],
    #     ['', '', '', '', '', ''],
    # ]

def iter_row(ws):
    for row in ws.iter_rows():
        yield [cell.value for cell in row]


def fill_output_kpi(filepath, arr):
    wr = load_workbook(filepath)
    sheet = wr['KPI']
    count = 0

    for i, row in enumerate(iter_row(sheet)):
        if count < 3:
            count += 1
            continue
        row = arr[i - 3]
        for j, cell in enumerate(row):
            j += 1
            sheet.cell(row=i, column=j).value = cell
    wr.save(filepath)


def main():
    standplaces, compatible_models, relocation_graph, orders, sources, stand_penalty, wagons = read_data()
    all_paths_for_wagons, all_paths_orders, all_paths_profit, paths_for_orders = generate_paths(wagons, orders, sources, relocation_graph, stand_penalty)
    
    model, completed_orders, selected_paths = solve_model(orders, wagons, paths_for_orders, all_paths_for_wagons, all_paths_profit, all_paths_orders)
    
    KPI_rows, orders_output, empty_moves = output_structures(completed_orders, selected_paths, orders, wagons, paths_for_orders, all_paths_for_wagons, all_paths_orders, stand_penalty, relocation_graph)
    
    
    
    orders_output.to_excel("output_wagon2.xlsx", sheet_name='Orders')  
    empty_moves.to_excel("output_wagon2.xlsx", sheet_name='emptyMoves')           
    fill_output_kpi('output_wagon2.xlsx', arr)
    
    f = open('KPI.txt','w')
    for row in KPI_rows:
        f.write(str(row) + '\n')
    f.close()
    
    
main()