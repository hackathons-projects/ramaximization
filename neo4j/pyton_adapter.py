from neo4j import GraphDatabase

class MetricsGraph:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    def print_metrics(self, start, finish, cost):
        with self.driver.session() as session:
            metrics = session.write_transaction(self._return_metrics, start, finish, cost)
            print(metrics)
            return metrics

    @staticmethod
    def _return_metrics(tx, start, finish, cost):
        result = tx.run(
            "MATCH (s:Metrics {Name: $start} ), (f:Metrics {Name: $finish}),"
            "p = shortestPath((s)-[rels:REL*]->(f)) "
            "WHERE (REDUCE(s = 0, r IN rels | s + TOINTEGER(r.Time)) < $cost)"
            "RETURN REDUCE(s = 0, r IN rels | s + TOINTEGER(r.Time))", start=start, finish=finish, cost=cost)
        return result.single()[0]


metrics_hash = {}


def get_metrics(start, finish, cost, host="bolt://localhost:7687", user="neo4j", pwd="admin"):
    if (start, finish) not in metrics_hash:
        metrics_class = MetricsGraph("bolt://localhost:7687", "neo4j", "admin")
        metrics = metrics_class.print_metrics(start, finish, cost)
        metrics_class.close()
        metrics_hash[(start, finish)] = metrics
    return metrics_hash[(start, finish)]


if __name__ == '__main__':
    get_metrics('37005', '32904', 25)
