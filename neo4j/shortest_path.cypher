MATCH (s:Metrics {Name: '37005'} ), (f:Metrics {Name: '32904'}),
p = shortestPath((s)-[rels:REL*]-(f))
WHERE REDUCE(s = 0, r IN rels | s + TOINTEGER(r.Time)) < 30
RETURN p,  REDUCE(s = 0, r IN rels | s + TOINTEGER(r.Time))